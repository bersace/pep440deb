#!/bin/bash -eux

python setup.py sdist
pep440deb --help
! pep440deb
pep440deb 1.0 | grep -q '^1.0$'
pep440deb --echo 1.0 | grep -q '^1.0 1.0$'
echo '1.0' | pep440deb --file | grep -q '^1.0$'
pep440deb --file <(echo '1.0') | grep -q '^1.0$'
pep440deb --echo --pypi pip
