import pytest


def test_debianize():
    from pep440deb import debianize, UserError

    assert '1.0' == debianize('1.0')
    assert '1.0~a1' == debianize('1.0a1')
    assert '1.0~a1' == debianize('1.0.a1')
    assert '1.0~a1' == debianize('1.0-alpha1')
    assert '1.0~a1~dev0' == debianize('1.0a1.dev0')

    with pytest.raises(UserError):
        debianize('1.0-svn1')
